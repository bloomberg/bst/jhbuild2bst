#!/bin/bash
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>
topdir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
gitdir="$(cd ${topdir}/.. && pwd)"

arg_export=
arg_schedule=
arg_track=
arg_modulesets=

function usage () {
    echo "Usage: "
    echo "  cronsetup.sh [OPTIONS]"
    echo
    echo "Setup a machine for automatically running jhbuild2bst"
    echo
    echo "Options:"
    echo
    echo "  -h --help                     Display this help message and exit"
    echo "  -e --export     <directory>   A directory that apache is serving"
    echo "  -m --modulesets <directory>   A modulesets checkout"
    echo "  -s --schedule   <expression>  A cron expression indicating when conversions should run"
    echo "  -t --track      <expression>  A cron expression indicating when bst track should run"
    echo
    echo "Note:"
    echo "  It is expected that the modulesets checkout has an upstream remote"
    echo "  configured to pull the latest master from in order to base conversions"
    echo "  on, and an origin remote which is where it will be pushing current branches to"
}

while : ; do
    case "$1" in
	-h|--help)
	    usage;
	    exit 0;
	    shift ;;

	-e|--export)
	    arg_export=${2}
	    shift 2 ;;

	-m|--modulesets)
	    arg_modulesets=${2}
	    shift 2 ;;

	-s|--schedule)
	    arg_schedule=${2}
	    shift 2 ;;

	-t|--track)
	    arg_track=${2}
	    shift 2 ;;

	*)
	    break ;;
    esac
done

if [ -z "${arg_schedule}" ]; then
    echo "Must specify the conversion schedule cron string"
    echo
    usage
    exit 1
fi

if [ -z "${arg_track}" ]; then
    echo "Must specify the track schedule cron string"
    echo
    usage
    exit 1
fi

if [ -z "${arg_modulesets}" ]; then
    echo "Must specify the modulesets checkout"
    echo
    usage
    exit 1
fi

if [ -z "${arg_export}" ]; then
    echo "Must specify the export directory"
    echo
    usage
    exit 1
fi

# Ensure the build schedule for either the convert-only
# task or the convert and bst track task
#
#  $1 - "convert" or "track"
#  $2 - The cron schedule string
#
function ensureBuildSchedule () {
    local jobtype=${1}
    local schedule=${2}
    local job=
    local scriptname="launch-autoconvert.sh"
    local add_options="--pushmaster"

    if [ "${jobtype}" == "track" ]; then
	scriptname="launch-autotrack.sh"
	add_options="--track"
    fi

    # Create the launch script based on our current configuration
    # and ensure that there is an entry in the user's crontab for
    # the launcher.
    #
    job="${schedule} ${topdir}/${scriptname}"
    sed -e "s|@@EXPORT@@|${arg_export}|g" \
        -e "s|@@TOPDIR@@|${topdir}|g" \
        -e "s|@@JOBTYPE@@|${jobtype}|g" \
        -e "s|@@JHBUILD2BST@@|${gitdir}|g" \
        -e "s|@@MODULESETS@@|${arg_modulesets}|g" \
        -e "s|@@ADDOPTS@@|${add_options}|g" \
	${topdir}/launcher.sh.in > ${topdir}/${scriptname}

    chmod +x ${topdir}/${scriptname}

    # Register the job with user's crontab
    cat <(fgrep -i -v "${scriptname}" <(crontab -l)) <(echo "$job") | crontab -
}

#
# Main
#

# Schedule or change schedule
ensureBuildSchedule "convert" "${arg_schedule}"
ensureBuildSchedule "track" "${arg_track}"

